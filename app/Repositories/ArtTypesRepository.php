<?php

namespace App\Repositories;

use App\Models\ArtTypes;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ArtTypesRepository
 * @package App\Repositories
 * @version November 30, 2018, 5:21 am UTC
 *
 * @method ArtTypes findWithoutFail($id, $columns = ['*'])
 * @method ArtTypes find($id, $columns = ['*'])
 * @method ArtTypes first($columns = ['*'])
*/
class ArtTypesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ArtTypes::class;
    }
}
