<?php

namespace App\Repositories;

use App\Models\Artist;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ArtistRepository
 * @package App\Repositories
 * @version November 30, 2018, 6:20 am UTC
 *
 * @method Artist findWithoutFail($id, $columns = ['*'])
 * @method Artist find($id, $columns = ['*'])
 * @method Artist first($columns = ['*'])
*/
class ArtistRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'firstname',
        'lastname',
        'nationality',
        'description',
        'image',
        'gallery_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Artist::class;
    }
}
