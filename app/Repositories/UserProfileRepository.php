<?php

namespace App\Repositories;

use App\Models\UserProfile;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UserProfileRepository
 * @package App\Repositories
 * @version November 30, 2018, 5:22 am UTC
 *
 * @method UserProfile findWithoutFail($id, $columns = ['*'])
 * @method UserProfile find($id, $columns = ['*'])
 * @method UserProfile first($columns = ['*'])
*/
class UserProfileRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'mobile',
        'location',
        'profession',
        'pricerange'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserProfile::class;
    }
}
