<?php

namespace App\Repositories;

use App\Models\GalleryArtist;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class GalleryArtistRepository
 * @package App\Repositories
 * @version November 30, 2018, 6:22 am UTC
 *
 * @method GalleryArtist findWithoutFail($id, $columns = ['*'])
 * @method GalleryArtist find($id, $columns = ['*'])
 * @method GalleryArtist first($columns = ['*'])
*/
class GalleryArtistRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'gallery_id',
        'artist_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return GalleryArtist::class;
    }
}
