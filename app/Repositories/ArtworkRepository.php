<?php

namespace App\Repositories;

use App\Models\Artwork;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ArtworkRepository
 * @package App\Repositories
 * @version November 30, 2018, 5:28 am UTC
 *
 * @method Artwork findWithoutFail($id, $columns = ['*'])
 * @method Artwork find($id, $columns = ['*'])
 * @method Artwork first($columns = ['*'])
*/
class ArtworkRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'artist_id',
        'art_type_id',
        'name',
        'title',
        'slug',
        'image',
        'price',
        'price_unit',
        'dateofcreation',
        'medium',
        'gallery_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Artwork::class;
    }
}
