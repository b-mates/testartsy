<?php

namespace App\Http\Controllers;

use App\DataTables\ArtworkDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateArtworkRequest;
use App\Http\Requests\UpdateArtworkRequest;
use App\Repositories\ArtworkRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ArtworkController extends AppBaseController
{
    /** @var  ArtworkRepository */
    private $artworkRepository;

    public function __construct(ArtworkRepository $artworkRepo)
    {
        $this->artworkRepository = $artworkRepo;
    }

    /**
     * Display a listing of the Artwork.
     *
     * @param ArtworkDataTable $artworkDataTable
     * @return Response
     */
    public function index(ArtworkDataTable $artworkDataTable)
    {
        return $artworkDataTable->render('artworks.index');
    }

    /**
     * Show the form for creating a new Artwork.
     *
     * @return Response
     */
    public function create()
    {
        return view('artworks.create');
    }

    /**
     * Store a newly created Artwork in storage.
     *
     * @param CreateArtworkRequest $request
     *
     * @return Response
     */
    public function store(CreateArtworkRequest $request)
    {
        $input = $request->all();

        $artwork = $this->artworkRepository->create($input);

        Flash::success('Artwork saved successfully.');

        return redirect(route('artworks.index'));
    }

    /**
     * Display the specified Artwork.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $artwork = $this->artworkRepository->findWithoutFail($id);

        if (empty($artwork)) {
            Flash::error('Artwork not found');

            return redirect(route('artworks.index'));
        }

        return view('artworks.show')->with('artwork', $artwork);
    }

    /**
     * Show the form for editing the specified Artwork.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $artwork = $this->artworkRepository->findWithoutFail($id);

        if (empty($artwork)) {
            Flash::error('Artwork not found');

            return redirect(route('artworks.index'));
        }

        return view('artworks.edit')->with('artwork', $artwork);
    }

    /**
     * Update the specified Artwork in storage.
     *
     * @param  int              $id
     * @param UpdateArtworkRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateArtworkRequest $request)
    {
        $artwork = $this->artworkRepository->findWithoutFail($id);

        if (empty($artwork)) {
            Flash::error('Artwork not found');

            return redirect(route('artworks.index'));
        }

        $artwork = $this->artworkRepository->update($request->all(), $id);

        Flash::success('Artwork updated successfully.');

        return redirect(route('artworks.index'));
    }

    /**
     * Remove the specified Artwork from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $artwork = $this->artworkRepository->findWithoutFail($id);

        if (empty($artwork)) {
            Flash::error('Artwork not found');

            return redirect(route('artworks.index'));
        }

        $this->artworkRepository->delete($id);

        Flash::success('Artwork deleted successfully.');

        return redirect(route('artworks.index'));
    }
}
