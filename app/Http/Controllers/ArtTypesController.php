<?php

namespace App\Http\Controllers;

use App\DataTables\ArtTypesDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateArtTypesRequest;
use App\Http\Requests\UpdateArtTypesRequest;
use App\Repositories\ArtTypesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ArtTypesController extends AppBaseController
{
    /** @var  ArtTypesRepository */
    private $artTypesRepository;

    public function __construct(ArtTypesRepository $artTypesRepo)
    {
        $this->artTypesRepository = $artTypesRepo;
    }

    /**
     * Display a listing of the ArtTypes.
     *
     * @param ArtTypesDataTable $artTypesDataTable
     * @return Response
     */
    public function index(ArtTypesDataTable $artTypesDataTable)
    {
        return $artTypesDataTable->render('art_types.index');
    }

    /**
     * Show the form for creating a new ArtTypes.
     *
     * @return Response
     */
    public function create()
    {
        return view('art_types.create');
    }

    /**
     * Store a newly created ArtTypes in storage.
     *
     * @param CreateArtTypesRequest $request
     *
     * @return Response
     */
    public function store(CreateArtTypesRequest $request)
    {
        $input = $request->all();

        $artTypes = $this->artTypesRepository->create($input);

        Flash::success('Art Types saved successfully.');

        return redirect(route('artTypes.index'));
    }

    /**
     * Display the specified ArtTypes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $artTypes = $this->artTypesRepository->findWithoutFail($id);

        if (empty($artTypes)) {
            Flash::error('Art Types not found');

            return redirect(route('artTypes.index'));
        }

        return view('art_types.show')->with('artTypes', $artTypes);
    }

    /**
     * Show the form for editing the specified ArtTypes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $artTypes = $this->artTypesRepository->findWithoutFail($id);

        if (empty($artTypes)) {
            Flash::error('Art Types not found');

            return redirect(route('artTypes.index'));
        }

        return view('art_types.edit')->with('artTypes', $artTypes);
    }

    /**
     * Update the specified ArtTypes in storage.
     *
     * @param  int              $id
     * @param UpdateArtTypesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateArtTypesRequest $request)
    {
        $artTypes = $this->artTypesRepository->findWithoutFail($id);

        if (empty($artTypes)) {
            Flash::error('Art Types not found');

            return redirect(route('artTypes.index'));
        }

        $artTypes = $this->artTypesRepository->update($request->all(), $id);

        Flash::success('Art Types updated successfully.');

        return redirect(route('artTypes.index'));
    }

    /**
     * Remove the specified ArtTypes from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $artTypes = $this->artTypesRepository->findWithoutFail($id);

        if (empty($artTypes)) {
            Flash::error('Art Types not found');

            return redirect(route('artTypes.index'));
        }

        $this->artTypesRepository->delete($id);

        Flash::success('Art Types deleted successfully.');

        return redirect(route('artTypes.index'));
    }
}
