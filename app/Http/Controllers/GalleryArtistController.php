<?php

namespace App\Http\Controllers;

use App\DataTables\GalleryArtistDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateGalleryArtistRequest;
use App\Http\Requests\UpdateGalleryArtistRequest;
use App\Repositories\GalleryArtistRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class GalleryArtistController extends AppBaseController
{
    /** @var  GalleryArtistRepository */
    private $galleryArtistRepository;

    public function __construct(GalleryArtistRepository $galleryArtistRepo)
    {
        $this->galleryArtistRepository = $galleryArtistRepo;
    }

    /**
     * Display a listing of the GalleryArtist.
     *
     * @param GalleryArtistDataTable $galleryArtistDataTable
     * @return Response
     */
    public function index(GalleryArtistDataTable $galleryArtistDataTable)
    {
        return $galleryArtistDataTable->render('gallery_artists.index');
    }

    /**
     * Show the form for creating a new GalleryArtist.
     *
     * @return Response
     */
    public function create()
    {
        return view('gallery_artists.create');
    }

    /**
     * Store a newly created GalleryArtist in storage.
     *
     * @param CreateGalleryArtistRequest $request
     *
     * @return Response
     */
    public function store(CreateGalleryArtistRequest $request)
    {
        $input = $request->all();

        $galleryArtist = $this->galleryArtistRepository->create($input);

        Flash::success('Gallery Artist saved successfully.');

        return redirect(route('galleryArtists.index'));
    }

    /**
     * Display the specified GalleryArtist.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $galleryArtist = $this->galleryArtistRepository->findWithoutFail($id);

        if (empty($galleryArtist)) {
            Flash::error('Gallery Artist not found');

            return redirect(route('galleryArtists.index'));
        }

        return view('gallery_artists.show')->with('galleryArtist', $galleryArtist);
    }

    /**
     * Show the form for editing the specified GalleryArtist.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $galleryArtist = $this->galleryArtistRepository->findWithoutFail($id);

        if (empty($galleryArtist)) {
            Flash::error('Gallery Artist not found');

            return redirect(route('galleryArtists.index'));
        }

        return view('gallery_artists.edit')->with('galleryArtist', $galleryArtist);
    }

    /**
     * Update the specified GalleryArtist in storage.
     *
     * @param  int              $id
     * @param UpdateGalleryArtistRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGalleryArtistRequest $request)
    {
        $galleryArtist = $this->galleryArtistRepository->findWithoutFail($id);

        if (empty($galleryArtist)) {
            Flash::error('Gallery Artist not found');

            return redirect(route('galleryArtists.index'));
        }

        $galleryArtist = $this->galleryArtistRepository->update($request->all(), $id);

        Flash::success('Gallery Artist updated successfully.');

        return redirect(route('galleryArtists.index'));
    }

    /**
     * Remove the specified GalleryArtist from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $galleryArtist = $this->galleryArtistRepository->findWithoutFail($id);

        if (empty($galleryArtist)) {
            Flash::error('Gallery Artist not found');

            return redirect(route('galleryArtists.index'));
        }

        $this->galleryArtistRepository->delete($id);

        Flash::success('Gallery Artist deleted successfully.');

        return redirect(route('galleryArtists.index'));
    }
}
