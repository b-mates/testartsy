<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateGalleryArtistAPIRequest;
use App\Http\Requests\API\UpdateGalleryArtistAPIRequest;
use App\Models\GalleryArtist;
use App\Repositories\GalleryArtistRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class GalleryArtistController
 * @package App\Http\Controllers\API
 */

class GalleryArtistAPIController extends AppBaseController
{
    /** @var  GalleryArtistRepository */
    private $galleryArtistRepository;

    public function __construct(GalleryArtistRepository $galleryArtistRepo)
    {
        $this->galleryArtistRepository = $galleryArtistRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/galleryArtists",
     *      summary="Get a listing of the GalleryArtists.",
     *      tags={"GalleryArtist"},
     *      description="Get all GalleryArtists",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/GalleryArtist")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->galleryArtistRepository->pushCriteria(new RequestCriteria($request));
        $this->galleryArtistRepository->pushCriteria(new LimitOffsetCriteria($request));
        $galleryArtists = $this->galleryArtistRepository->all();

        return $this->sendResponse($galleryArtists->toArray(), 'Gallery Artists retrieved successfully');
    }

    /**
     * @param CreateGalleryArtistAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/galleryArtists",
     *      summary="Store a newly created GalleryArtist in storage",
     *      tags={"GalleryArtist"},
     *      description="Store GalleryArtist",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="GalleryArtist that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/GalleryArtist")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/GalleryArtist"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateGalleryArtistAPIRequest $request)
    {
        $input = $request->all();

        $galleryArtists = $this->galleryArtistRepository->create($input);

        return $this->sendResponse($galleryArtists->toArray(), 'Gallery Artist saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/galleryArtists/{id}",
     *      summary="Display the specified GalleryArtist",
     *      tags={"GalleryArtist"},
     *      description="Get GalleryArtist",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of GalleryArtist",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/GalleryArtist"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var GalleryArtist $galleryArtist */
        $galleryArtist = $this->galleryArtistRepository->findWithoutFail($id);

        if (empty($galleryArtist)) {
            return $this->sendError('Gallery Artist not found');
        }

        return $this->sendResponse($galleryArtist->toArray(), 'Gallery Artist retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateGalleryArtistAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/galleryArtists/{id}",
     *      summary="Update the specified GalleryArtist in storage",
     *      tags={"GalleryArtist"},
     *      description="Update GalleryArtist",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of GalleryArtist",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="GalleryArtist that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/GalleryArtist")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/GalleryArtist"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateGalleryArtistAPIRequest $request)
    {
        $input = $request->all();

        /** @var GalleryArtist $galleryArtist */
        $galleryArtist = $this->galleryArtistRepository->findWithoutFail($id);

        if (empty($galleryArtist)) {
            return $this->sendError('Gallery Artist not found');
        }

        $galleryArtist = $this->galleryArtistRepository->update($input, $id);

        return $this->sendResponse($galleryArtist->toArray(), 'GalleryArtist updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/galleryArtists/{id}",
     *      summary="Remove the specified GalleryArtist from storage",
     *      tags={"GalleryArtist"},
     *      description="Delete GalleryArtist",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of GalleryArtist",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var GalleryArtist $galleryArtist */
        $galleryArtist = $this->galleryArtistRepository->findWithoutFail($id);

        if (empty($galleryArtist)) {
            return $this->sendError('Gallery Artist not found');
        }

        $galleryArtist->delete();

        return $this->sendResponse($id, 'Gallery Artist deleted successfully');
    }
}
