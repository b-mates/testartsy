<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateGalleryAPIRequest;
use App\Http\Requests\API\UpdateGalleryAPIRequest;
use App\Models\Gallery;
use App\Repositories\GalleryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class GalleryController
 * @package App\Http\Controllers\API
 */

class GalleryAPIController extends AppBaseController
{
    /** @var  GalleryRepository */
    private $galleryRepository;

    public function __construct(GalleryRepository $galleryRepo)
    {
        $this->galleryRepository = $galleryRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/galleries",
     *      summary="Get a listing of the Galleries.",
     *      tags={"Gallery"},
     *      description="Get all Galleries",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Gallery")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->galleryRepository->pushCriteria(new RequestCriteria($request));
        $this->galleryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $galleries = $this->galleryRepository->all();

        return $this->sendResponse($galleries->toArray(), 'Galleries retrieved successfully');
    }

    /**
     * @param CreateGalleryAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/galleries",
     *      summary="Store a newly created Gallery in storage",
     *      tags={"Gallery"},
     *      description="Store Gallery",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Gallery that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Gallery")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Gallery"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateGalleryAPIRequest $request)
    {
        $input = $request->all();

        $galleries = $this->galleryRepository->create($input);

        return $this->sendResponse($galleries->toArray(), 'Gallery saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/galleries/{id}",
     *      summary="Display the specified Gallery",
     *      tags={"Gallery"},
     *      description="Get Gallery",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Gallery",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Gallery"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Gallery $gallery */
        $gallery = $this->galleryRepository->findWithoutFail($id);

        if (empty($gallery)) {
            return $this->sendError('Gallery not found');
        }

        return $this->sendResponse($gallery->toArray(), 'Gallery retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateGalleryAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/galleries/{id}",
     *      summary="Update the specified Gallery in storage",
     *      tags={"Gallery"},
     *      description="Update Gallery",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Gallery",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Gallery that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Gallery")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Gallery"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateGalleryAPIRequest $request)
    {
        $input = $request->all();

        /** @var Gallery $gallery */
        $gallery = $this->galleryRepository->findWithoutFail($id);

        if (empty($gallery)) {
            return $this->sendError('Gallery not found');
        }

        $gallery = $this->galleryRepository->update($input, $id);

        return $this->sendResponse($gallery->toArray(), 'Gallery updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/galleries/{id}",
     *      summary="Remove the specified Gallery from storage",
     *      tags={"Gallery"},
     *      description="Delete Gallery",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Gallery",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Gallery $gallery */
        $gallery = $this->galleryRepository->findWithoutFail($id);

        if (empty($gallery)) {
            return $this->sendError('Gallery not found');
        }

        $gallery->delete();

        return $this->sendResponse($id, 'Gallery deleted successfully');
    }
}
