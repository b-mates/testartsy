<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateArtTypesAPIRequest;
use App\Http\Requests\API\UpdateArtTypesAPIRequest;
use App\Models\ArtTypes;
use App\Repositories\ArtTypesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ArtTypesController
 * @package App\Http\Controllers\API
 */

class ArtTypesAPIController extends AppBaseController
{
    /** @var  ArtTypesRepository */
    private $artTypesRepository;

    public function __construct(ArtTypesRepository $artTypesRepo)
    {
        $this->artTypesRepository = $artTypesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/artTypes",
     *      summary="Get a listing of the ArtTypes.",
     *      tags={"ArtTypes"},
     *      description="Get all ArtTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ArtTypes")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->artTypesRepository->pushCriteria(new RequestCriteria($request));
        $this->artTypesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $artTypes = $this->artTypesRepository->all();

        return $this->sendResponse($artTypes->toArray(), 'Art Types retrieved successfully');
    }

    /**
     * @param CreateArtTypesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/artTypes",
     *      summary="Store a newly created ArtTypes in storage",
     *      tags={"ArtTypes"},
     *      description="Store ArtTypes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ArtTypes that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ArtTypes")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ArtTypes"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateArtTypesAPIRequest $request)
    {
        $input = $request->all();

        $artTypes = $this->artTypesRepository->create($input);

        return $this->sendResponse($artTypes->toArray(), 'Art Types saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/artTypes/{id}",
     *      summary="Display the specified ArtTypes",
     *      tags={"ArtTypes"},
     *      description="Get ArtTypes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ArtTypes",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ArtTypes"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var ArtTypes $artTypes */
        $artTypes = $this->artTypesRepository->findWithoutFail($id);

        if (empty($artTypes)) {
            return $this->sendError('Art Types not found');
        }

        return $this->sendResponse($artTypes->toArray(), 'Art Types retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateArtTypesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/artTypes/{id}",
     *      summary="Update the specified ArtTypes in storage",
     *      tags={"ArtTypes"},
     *      description="Update ArtTypes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ArtTypes",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ArtTypes that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ArtTypes")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ArtTypes"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateArtTypesAPIRequest $request)
    {
        $input = $request->all();

        /** @var ArtTypes $artTypes */
        $artTypes = $this->artTypesRepository->findWithoutFail($id);

        if (empty($artTypes)) {
            return $this->sendError('Art Types not found');
        }

        $artTypes = $this->artTypesRepository->update($input, $id);

        return $this->sendResponse($artTypes->toArray(), 'ArtTypes updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/artTypes/{id}",
     *      summary="Remove the specified ArtTypes from storage",
     *      tags={"ArtTypes"},
     *      description="Delete ArtTypes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ArtTypes",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ArtTypes $artTypes */
        $artTypes = $this->artTypesRepository->findWithoutFail($id);

        if (empty($artTypes)) {
            return $this->sendError('Art Types not found');
        }

        $artTypes->delete();

        return $this->sendResponse($id, 'Art Types deleted successfully');
    }
}
