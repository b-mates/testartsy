<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateArtworkAPIRequest;
use App\Http\Requests\API\UpdateArtworkAPIRequest;
use App\Models\Artwork;
use App\Repositories\ArtworkRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ArtworkController
 * @package App\Http\Controllers\API
 */

class ArtworkAPIController extends AppBaseController
{
    /** @var  ArtworkRepository */
    private $artworkRepository;

    public function __construct(ArtworkRepository $artworkRepo)
    {
        $this->artworkRepository = $artworkRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/artworks",
     *      summary="Get a listing of the Artworks.",
     *      tags={"Artwork"},
     *      description="Get all Artworks",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Artwork")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->artworkRepository->pushCriteria(new RequestCriteria($request));
        $this->artworkRepository->pushCriteria(new LimitOffsetCriteria($request));
        $artworks = $this->artworkRepository->all();

        return $this->sendResponse($artworks->toArray(), 'Artworks retrieved successfully');
    }

    /**
     * @param CreateArtworkAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/artworks",
     *      summary="Store a newly created Artwork in storage",
     *      tags={"Artwork"},
     *      description="Store Artwork",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Artwork that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Artwork")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Artwork"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateArtworkAPIRequest $request)
    {
        $input = $request->all();

        $artworks = $this->artworkRepository->create($input);

        return $this->sendResponse($artworks->toArray(), 'Artwork saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/artworks/{id}",
     *      summary="Display the specified Artwork",
     *      tags={"Artwork"},
     *      description="Get Artwork",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Artwork",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Artwork"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Artwork $artwork */
        $artwork = $this->artworkRepository->findWithoutFail($id);

        if (empty($artwork)) {
            return $this->sendError('Artwork not found');
        }

        return $this->sendResponse($artwork->toArray(), 'Artwork retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateArtworkAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/artworks/{id}",
     *      summary="Update the specified Artwork in storage",
     *      tags={"Artwork"},
     *      description="Update Artwork",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Artwork",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Artwork that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Artwork")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Artwork"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateArtworkAPIRequest $request)
    {
        $input = $request->all();

        /** @var Artwork $artwork */
        $artwork = $this->artworkRepository->findWithoutFail($id);

        if (empty($artwork)) {
            return $this->sendError('Artwork not found');
        }

        $artwork = $this->artworkRepository->update($input, $id);

        return $this->sendResponse($artwork->toArray(), 'Artwork updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/artworks/{id}",
     *      summary="Remove the specified Artwork from storage",
     *      tags={"Artwork"},
     *      description="Delete Artwork",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Artwork",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Artwork $artwork */
        $artwork = $this->artworkRepository->findWithoutFail($id);

        if (empty($artwork)) {
            return $this->sendError('Artwork not found');
        }

        $artwork->delete();

        return $this->sendResponse($id, 'Artwork deleted successfully');
    }
}
