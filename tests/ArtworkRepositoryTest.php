<?php

use App\Models\Artwork;
use App\Repositories\ArtworkRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ArtworkRepositoryTest extends TestCase
{
    use MakeArtworkTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ArtworkRepository
     */
    protected $artworkRepo;

    public function setUp()
    {
        parent::setUp();
        $this->artworkRepo = App::make(ArtworkRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateArtwork()
    {
        $artwork = $this->fakeArtworkData();
        $createdArtwork = $this->artworkRepo->create($artwork);
        $createdArtwork = $createdArtwork->toArray();
        $this->assertArrayHasKey('id', $createdArtwork);
        $this->assertNotNull($createdArtwork['id'], 'Created Artwork must have id specified');
        $this->assertNotNull(Artwork::find($createdArtwork['id']), 'Artwork with given id must be in DB');
        $this->assertModelData($artwork, $createdArtwork);
    }

    /**
     * @test read
     */
    public function testReadArtwork()
    {
        $artwork = $this->makeArtwork();
        $dbArtwork = $this->artworkRepo->find($artwork->id);
        $dbArtwork = $dbArtwork->toArray();
        $this->assertModelData($artwork->toArray(), $dbArtwork);
    }

    /**
     * @test update
     */
    public function testUpdateArtwork()
    {
        $artwork = $this->makeArtwork();
        $fakeArtwork = $this->fakeArtworkData();
        $updatedArtwork = $this->artworkRepo->update($fakeArtwork, $artwork->id);
        $this->assertModelData($fakeArtwork, $updatedArtwork->toArray());
        $dbArtwork = $this->artworkRepo->find($artwork->id);
        $this->assertModelData($fakeArtwork, $dbArtwork->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteArtwork()
    {
        $artwork = $this->makeArtwork();
        $resp = $this->artworkRepo->delete($artwork->id);
        $this->assertTrue($resp);
        $this->assertNull(Artwork::find($artwork->id), 'Artwork should not exist in DB');
    }
}
