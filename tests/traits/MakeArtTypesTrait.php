<?php

use Faker\Factory as Faker;
use App\Models\ArtTypes;
use App\Repositories\ArtTypesRepository;

trait MakeArtTypesTrait
{
    /**
     * Create fake instance of ArtTypes and save it in database
     *
     * @param array $artTypesFields
     * @return ArtTypes
     */
    public function makeArtTypes($artTypesFields = [])
    {
        /** @var ArtTypesRepository $artTypesRepo */
        $artTypesRepo = App::make(ArtTypesRepository::class);
        $theme = $this->fakeArtTypesData($artTypesFields);
        return $artTypesRepo->create($theme);
    }

    /**
     * Get fake instance of ArtTypes
     *
     * @param array $artTypesFields
     * @return ArtTypes
     */
    public function fakeArtTypes($artTypesFields = [])
    {
        return new ArtTypes($this->fakeArtTypesData($artTypesFields));
    }

    /**
     * Get fake data of ArtTypes
     *
     * @param array $postFields
     * @return array
     */
    public function fakeArtTypesData($artTypesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word
        ], $artTypesFields);
    }
}
