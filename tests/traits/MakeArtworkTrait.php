<?php

use Faker\Factory as Faker;
use App\Models\Artwork;
use App\Repositories\ArtworkRepository;

trait MakeArtworkTrait
{
    /**
     * Create fake instance of Artwork and save it in database
     *
     * @param array $artworkFields
     * @return Artwork
     */
    public function makeArtwork($artworkFields = [])
    {
        /** @var ArtworkRepository $artworkRepo */
        $artworkRepo = App::make(ArtworkRepository::class);
        $theme = $this->fakeArtworkData($artworkFields);
        return $artworkRepo->create($theme);
    }

    /**
     * Get fake instance of Artwork
     *
     * @param array $artworkFields
     * @return Artwork
     */
    public function fakeArtwork($artworkFields = [])
    {
        return new Artwork($this->fakeArtworkData($artworkFields));
    }

    /**
     * Get fake data of Artwork
     *
     * @param array $postFields
     * @return array
     */
    public function fakeArtworkData($artworkFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'artist_id' => $fake->randomDigitNotNull,
            'art_type_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'title' => $fake->word,
            'slug' => $fake->word,
            'image' => $fake->word,
            'price' => $fake->word,
            'price_unit' => $fake->word,
            'dateofcreation' => $fake->date('Y-m-d H:i:s'),
            'medium' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'gallery_id' => $fake->randomDigitNotNull
        ], $artworkFields);
    }
}
