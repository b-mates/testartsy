<?php

use Faker\Factory as Faker;
use App\Models\GalleryArtist;
use App\Repositories\GalleryArtistRepository;

trait MakeGalleryArtistTrait
{
    /**
     * Create fake instance of GalleryArtist and save it in database
     *
     * @param array $galleryArtistFields
     * @return GalleryArtist
     */
    public function makeGalleryArtist($galleryArtistFields = [])
    {
        /** @var GalleryArtistRepository $galleryArtistRepo */
        $galleryArtistRepo = App::make(GalleryArtistRepository::class);
        $theme = $this->fakeGalleryArtistData($galleryArtistFields);
        return $galleryArtistRepo->create($theme);
    }

    /**
     * Get fake instance of GalleryArtist
     *
     * @param array $galleryArtistFields
     * @return GalleryArtist
     */
    public function fakeGalleryArtist($galleryArtistFields = [])
    {
        return new GalleryArtist($this->fakeGalleryArtistData($galleryArtistFields));
    }

    /**
     * Get fake data of GalleryArtist
     *
     * @param array $postFields
     * @return array
     */
    public function fakeGalleryArtistData($galleryArtistFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'gallery_id' => $fake->randomDigitNotNull,
            'artist_id' => $fake->randomDigitNotNull
        ], $galleryArtistFields);
    }
}
