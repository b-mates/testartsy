<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ArtTypesApiTest extends TestCase
{
    use MakeArtTypesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateArtTypes()
    {
        $artTypes = $this->fakeArtTypesData();
        $this->json('POST', '/api/v1/artTypes', $artTypes);

        $this->assertApiResponse($artTypes);
    }

    /**
     * @test
     */
    public function testReadArtTypes()
    {
        $artTypes = $this->makeArtTypes();
        $this->json('GET', '/api/v1/artTypes/'.$artTypes->id);

        $this->assertApiResponse($artTypes->toArray());
    }

    /**
     * @test
     */
    public function testUpdateArtTypes()
    {
        $artTypes = $this->makeArtTypes();
        $editedArtTypes = $this->fakeArtTypesData();

        $this->json('PUT', '/api/v1/artTypes/'.$artTypes->id, $editedArtTypes);

        $this->assertApiResponse($editedArtTypes);
    }

    /**
     * @test
     */
    public function testDeleteArtTypes()
    {
        $artTypes = $this->makeArtTypes();
        $this->json('DELETE', '/api/v1/artTypes/'.$artTypes->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/artTypes/'.$artTypes->id);

        $this->assertResponseStatus(404);
    }
}
