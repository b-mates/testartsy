<?php

use App\Models\GalleryArtist;
use App\Repositories\GalleryArtistRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GalleryArtistRepositoryTest extends TestCase
{
    use MakeGalleryArtistTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var GalleryArtistRepository
     */
    protected $galleryArtistRepo;

    public function setUp()
    {
        parent::setUp();
        $this->galleryArtistRepo = App::make(GalleryArtistRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateGalleryArtist()
    {
        $galleryArtist = $this->fakeGalleryArtistData();
        $createdGalleryArtist = $this->galleryArtistRepo->create($galleryArtist);
        $createdGalleryArtist = $createdGalleryArtist->toArray();
        $this->assertArrayHasKey('id', $createdGalleryArtist);
        $this->assertNotNull($createdGalleryArtist['id'], 'Created GalleryArtist must have id specified');
        $this->assertNotNull(GalleryArtist::find($createdGalleryArtist['id']), 'GalleryArtist with given id must be in DB');
        $this->assertModelData($galleryArtist, $createdGalleryArtist);
    }

    /**
     * @test read
     */
    public function testReadGalleryArtist()
    {
        $galleryArtist = $this->makeGalleryArtist();
        $dbGalleryArtist = $this->galleryArtistRepo->find($galleryArtist->id);
        $dbGalleryArtist = $dbGalleryArtist->toArray();
        $this->assertModelData($galleryArtist->toArray(), $dbGalleryArtist);
    }

    /**
     * @test update
     */
    public function testUpdateGalleryArtist()
    {
        $galleryArtist = $this->makeGalleryArtist();
        $fakeGalleryArtist = $this->fakeGalleryArtistData();
        $updatedGalleryArtist = $this->galleryArtistRepo->update($fakeGalleryArtist, $galleryArtist->id);
        $this->assertModelData($fakeGalleryArtist, $updatedGalleryArtist->toArray());
        $dbGalleryArtist = $this->galleryArtistRepo->find($galleryArtist->id);
        $this->assertModelData($fakeGalleryArtist, $dbGalleryArtist->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteGalleryArtist()
    {
        $galleryArtist = $this->makeGalleryArtist();
        $resp = $this->galleryArtistRepo->delete($galleryArtist->id);
        $this->assertTrue($resp);
        $this->assertNull(GalleryArtist::find($galleryArtist->id), 'GalleryArtist should not exist in DB');
    }
}
