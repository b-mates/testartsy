<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ArtworkApiTest extends TestCase
{
    use MakeArtworkTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateArtwork()
    {
        $artwork = $this->fakeArtworkData();
        $this->json('POST', '/api/v1/artworks', $artwork);

        $this->assertApiResponse($artwork);
    }

    /**
     * @test
     */
    public function testReadArtwork()
    {
        $artwork = $this->makeArtwork();
        $this->json('GET', '/api/v1/artworks/'.$artwork->id);

        $this->assertApiResponse($artwork->toArray());
    }

    /**
     * @test
     */
    public function testUpdateArtwork()
    {
        $artwork = $this->makeArtwork();
        $editedArtwork = $this->fakeArtworkData();

        $this->json('PUT', '/api/v1/artworks/'.$artwork->id, $editedArtwork);

        $this->assertApiResponse($editedArtwork);
    }

    /**
     * @test
     */
    public function testDeleteArtwork()
    {
        $artwork = $this->makeArtwork();
        $this->json('DELETE', '/api/v1/artworks/'.$artwork->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/artworks/'.$artwork->id);

        $this->assertResponseStatus(404);
    }
}
