<?php

use App\Models\ArtTypes;
use App\Repositories\ArtTypesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ArtTypesRepositoryTest extends TestCase
{
    use MakeArtTypesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ArtTypesRepository
     */
    protected $artTypesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->artTypesRepo = App::make(ArtTypesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateArtTypes()
    {
        $artTypes = $this->fakeArtTypesData();
        $createdArtTypes = $this->artTypesRepo->create($artTypes);
        $createdArtTypes = $createdArtTypes->toArray();
        $this->assertArrayHasKey('id', $createdArtTypes);
        $this->assertNotNull($createdArtTypes['id'], 'Created ArtTypes must have id specified');
        $this->assertNotNull(ArtTypes::find($createdArtTypes['id']), 'ArtTypes with given id must be in DB');
        $this->assertModelData($artTypes, $createdArtTypes);
    }

    /**
     * @test read
     */
    public function testReadArtTypes()
    {
        $artTypes = $this->makeArtTypes();
        $dbArtTypes = $this->artTypesRepo->find($artTypes->id);
        $dbArtTypes = $dbArtTypes->toArray();
        $this->assertModelData($artTypes->toArray(), $dbArtTypes);
    }

    /**
     * @test update
     */
    public function testUpdateArtTypes()
    {
        $artTypes = $this->makeArtTypes();
        $fakeArtTypes = $this->fakeArtTypesData();
        $updatedArtTypes = $this->artTypesRepo->update($fakeArtTypes, $artTypes->id);
        $this->assertModelData($fakeArtTypes, $updatedArtTypes->toArray());
        $dbArtTypes = $this->artTypesRepo->find($artTypes->id);
        $this->assertModelData($fakeArtTypes, $dbArtTypes->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteArtTypes()
    {
        $artTypes = $this->makeArtTypes();
        $resp = $this->artTypesRepo->delete($artTypes->id);
        $this->assertTrue($resp);
        $this->assertNull(ArtTypes::find($artTypes->id), 'ArtTypes should not exist in DB');
    }
}
