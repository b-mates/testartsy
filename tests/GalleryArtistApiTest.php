<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GalleryArtistApiTest extends TestCase
{
    use MakeGalleryArtistTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateGalleryArtist()
    {
        $galleryArtist = $this->fakeGalleryArtistData();
        $this->json('POST', '/api/v1/galleryArtists', $galleryArtist);

        $this->assertApiResponse($galleryArtist);
    }

    /**
     * @test
     */
    public function testReadGalleryArtist()
    {
        $galleryArtist = $this->makeGalleryArtist();
        $this->json('GET', '/api/v1/galleryArtists/'.$galleryArtist->id);

        $this->assertApiResponse($galleryArtist->toArray());
    }

    /**
     * @test
     */
    public function testUpdateGalleryArtist()
    {
        $galleryArtist = $this->makeGalleryArtist();
        $editedGalleryArtist = $this->fakeGalleryArtistData();

        $this->json('PUT', '/api/v1/galleryArtists/'.$galleryArtist->id, $editedGalleryArtist);

        $this->assertApiResponse($editedGalleryArtist);
    }

    /**
     * @test
     */
    public function testDeleteGalleryArtist()
    {
        $galleryArtist = $this->makeGalleryArtist();
        $this->json('DELETE', '/api/v1/galleryArtists/'.$galleryArtist->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/galleryArtists/'.$galleryArtist->id);

        $this->assertResponseStatus(404);
    }
}
