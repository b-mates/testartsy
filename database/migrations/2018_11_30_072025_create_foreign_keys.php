<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('artists', function(Blueprint $table) {
			$table->foreign('gallery_id')->references('id')->on('galleries')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('galleries', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('artworks', function(Blueprint $table) {
			$table->foreign('artist_id')->references('id')->on('artists')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('artworks', function(Blueprint $table) {
			$table->foreign('art_type_id')->references('id')->on('art_types')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('artworks', function(Blueprint $table) {
			$table->foreign('gallery_id')->references('id')->on('galleries')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
//		Schema::table('user_profiles', function(Blueprint $table) {
//			$table->foreign('user_id')->references('id')->on('users')
//						->onDelete('cascade')
//						->onUpdate('cascade');
//		});
		Schema::table('gallery_artists', function(Blueprint $table) {
			$table->foreign('gallery_id')->references('id')->on('galleries')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('gallery_artists', function(Blueprint $table) {
			$table->foreign('artist_id')->references('id')->on('artists')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('artists', function(Blueprint $table) {
			$table->dropForeign('artists_gallery_id_foreign');
		});
		Schema::table('galleries', function(Blueprint $table) {
			$table->dropForeign('galleries_user_id_foreign');
		});
		Schema::table('artworks', function(Blueprint $table) {
			$table->dropForeign('artworks_artist_id_foreign');
		});
		Schema::table('artworks', function(Blueprint $table) {
			$table->dropForeign('artworks_art_type_id_foreign');
		});
		Schema::table('artworks', function(Blueprint $table) {
			$table->dropForeign('artworks_gallery_id_foreign');
		});
//		Schema::table('user_profiles', function(Blueprint $table) {
//			$table->dropForeign('user_profiles_user_id_foreign');
//		});
		Schema::table('gallery_artists', function(Blueprint $table) {
			$table->dropForeign('gallery_artists_gallery_id_foreign');
		});
		Schema::table('gallery_artists', function(Blueprint $table) {
			$table->dropForeign('gallery_artists_artist_id_foreign');
		});
	}
}