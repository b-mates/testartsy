<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserProfilesTable extends Migration {

	public function up()
	{
		Schema::create('user_profiles', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->string('mobile', 15)->nullable();
			$table->string('location', 300)->nullable();
			$table->string('profession', 200)->nullable();
			$table->bigInteger('pricerange')->nullable();
			$table->timestamps();
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('user_profiles');
	}
}