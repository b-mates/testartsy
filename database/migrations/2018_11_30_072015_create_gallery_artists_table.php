<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGalleryArtistsTable extends Migration {

	public function up()
	{
		Schema::create('gallery_artists', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('gallery_id')->unsigned();
			$table->integer('artist_id')->unsigned();
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('gallery_artists');
	}
}