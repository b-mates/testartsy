<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArtworksTable extends Migration {

	public function up()
	{
		Schema::create('artworks', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('artist_id')->unsigned();
			$table->integer('art_type_id')->unsigned();
			$table->string('name', 300);
			$table->string('title', 300);
			$table->string('slug', 300);
			$table->string('image', 300);
			$table->bigInteger('price');
			$table->string('price_unit', 3);
			$table->datetime('dateofcreation');
			$table->string('medium', 300)->nullable();
			$table->timestamps();
			$table->integer('gallery_id')->unsigned();
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('artworks');
	}
}