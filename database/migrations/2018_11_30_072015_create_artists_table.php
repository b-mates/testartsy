<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArtistsTable extends Migration {

	public function up()
	{
		Schema::create('artists', function(Blueprint $table) {
			$table->increments('id');
			$table->string('firstname', 200);
			$table->string('lastname', 200);
			$table->string('nationality', 200);
			$table->longText('description');
			$table->string('image', 300)->nullable();
			$table->timestamps();
			$table->integer('gallery_id')->unsigned();
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('artists');
	}
}