<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGalleriesTable extends Migration {

	public function up()
	{
		Schema::create('galleries', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 200);
			$table->string('addressline1', 300);
			$table->string('addressline2', 300)->nullable();
			$table->string('city', 100);
			$table->string('region', 100);
			$table->longText('about');
			$table->bigInteger('starting_date');
			$table->bigInteger('ending_date');
			$table->string('image', 300);
			$table->timestamps();
			$table->integer('user_id')->unsigned();
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('galleries');
	}
}