<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArtTypesTable extends Migration {

	public function up()
	{
		Schema::create('art_types', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 300);
			$table->timestamps();
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('art_types');
	}
}