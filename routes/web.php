<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder');

Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate');

Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate');



Route::resource('artTypes', 'ArtTypesController');

Route::resource('userProfiles', 'UserProfileController');

Route::resource('galleries', 'GalleryController');

Route::resource('artworks', 'ArtworkController');

Route::resource('artists', 'ArtistController');

Route::resource('users', 'UserController');

Route::resource('galleryArtists', 'GalleryArtistController');