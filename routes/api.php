<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'AuthController@login');

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('logout', 'AuthController@logout'); 
    Route::get('me', 'AuthController@me');
});

//Route::resource('users', 'UserAPIController');

Route::resource('art_types', 'ArtTypesAPIController');

Route::resource('user_profiles', 'UserProfileAPIController');

Route::resource('galleries', 'GalleryAPIController');

Route::resource('artworks', 'ArtworkAPIController');

Route::resource('artists', 'ArtistAPIController');

Route::resource('users', 'UserAPIController');

Route::resource('gallery_artists', 'GalleryArtistAPIController');