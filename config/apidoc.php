<?php
return [
    'apiVersion'     => '1.0.0',
    'apiTitle'       => 'Artsy',
    'apiDescription' => 'This is the API for Artsy project',
    'apiBasePath'    => '/api/v1',
    'authorization'  => 'jwt',
];
