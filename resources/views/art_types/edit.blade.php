@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Art Types
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($artTypes, ['route' => ['artTypes.update', $artTypes->id], 'method' => 'patch']) !!}

                        @include('art_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection