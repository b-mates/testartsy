<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $artwork->id !!}</p>
</div>

<!-- Artist Id Field -->
<div class="form-group">
    {!! Form::label('artist_id', 'Artist Id:') !!}
    <p>{!! $artwork->artist_id !!}</p>
</div>

<!-- Art Type Id Field -->
<div class="form-group">
    {!! Form::label('art_type_id', 'Art Type Id:') !!}
    <p>{!! $artwork->art_type_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $artwork->name !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $artwork->title !!}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{!! $artwork->slug !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $artwork->image !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $artwork->price !!}</p>
</div>

<!-- Price Unit Field -->
<div class="form-group">
    {!! Form::label('price_unit', 'Price Unit:') !!}
    <p>{!! $artwork->price_unit !!}</p>
</div>

<!-- Dateofcreation Field -->
<div class="form-group">
    {!! Form::label('dateofcreation', 'Dateofcreation:') !!}
    <p>{!! $artwork->dateofcreation !!}</p>
</div>

<!-- Medium Field -->
<div class="form-group">
    {!! Form::label('medium', 'Medium:') !!}
    <p>{!! $artwork->medium !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $artwork->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $artwork->updated_at !!}</p>
</div>

<!-- Gallery Id Field -->
<div class="form-group">
    {!! Form::label('gallery_id', 'Gallery Id:') !!}
    <p>{!! $artwork->gallery_id !!}</p>
</div>

