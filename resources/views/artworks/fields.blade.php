<!-- Artist Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('artist_id', 'Artist Id:') !!}
    {!! Form::number('artist_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Art Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('art_type_id', 'Art Type Id:') !!}
    {!! Form::number('art_type_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::text('image', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Unit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price_unit', 'Price Unit:') !!}
    {!! Form::text('price_unit', null, ['class' => 'form-control']) !!}
</div>

<!-- Dateofcreation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dateofcreation', 'Dateofcreation:') !!}
    {!! Form::date('dateofcreation', null, ['class' => 'form-control']) !!}
</div>

<!-- Medium Field -->
<div class="form-group col-sm-6">
    {!! Form::label('medium', 'Medium:') !!}
    {!! Form::text('medium', null, ['class' => 'form-control']) !!}
</div>

<!-- Gallery Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gallery_id', 'Gallery Id:') !!}
    {!! Form::number('gallery_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('artworks.index') !!}" class="btn btn-default">Cancel</a>
</div>
