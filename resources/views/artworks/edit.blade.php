@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Artwork
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($artwork, ['route' => ['artworks.update', $artwork->id], 'method' => 'patch']) !!}

                        @include('artworks.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection