<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $gallery->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $gallery->name !!}</p>
</div>

<!-- Addressline1 Field -->
<div class="form-group">
    {!! Form::label('addressline1', 'Addressline1:') !!}
    <p>{!! $gallery->addressline1 !!}</p>
</div>

<!-- Addressline2 Field -->
<div class="form-group">
    {!! Form::label('addressline2', 'Addressline2:') !!}
    <p>{!! $gallery->addressline2 !!}</p>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'City:') !!}
    <p>{!! $gallery->city !!}</p>
</div>

<!-- Region Field -->
<div class="form-group">
    {!! Form::label('region', 'Region:') !!}
    <p>{!! $gallery->region !!}</p>
</div>

<!-- About Field -->
<div class="form-group">
    {!! Form::label('about', 'About:') !!}
    <p>{!! $gallery->about !!}</p>
</div>

<!-- Starting Date Field -->
<div class="form-group">
    {!! Form::label('starting_date', 'Starting Date:') !!}
    <p>{!! $gallery->starting_date !!}</p>
</div>

<!-- Ending Date Field -->
<div class="form-group">
    {!! Form::label('ending_date', 'Ending Date:') !!}
    <p>{!! $gallery->ending_date !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $gallery->image !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $gallery->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $gallery->updated_at !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $gallery->user_id !!}</p>
</div>

