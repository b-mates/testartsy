<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $userProfile->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $userProfile->user_id !!}</p>
</div>

<!-- Mobile Field -->
<div class="form-group">
    {!! Form::label('mobile', 'Mobile:') !!}
    <p>{!! $userProfile->mobile !!}</p>
</div>

<!-- Location Field -->
<div class="form-group">
    {!! Form::label('location', 'Location:') !!}
    <p>{!! $userProfile->location !!}</p>
</div>

<!-- Profession Field -->
<div class="form-group">
    {!! Form::label('profession', 'Profession:') !!}
    <p>{!! $userProfile->profession !!}</p>
</div>

<!-- Pricerange Field -->
<div class="form-group">
    {!! Form::label('pricerange', 'Pricerange:') !!}
    <p>{!! $userProfile->pricerange !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $userProfile->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $userProfile->updated_at !!}</p>
</div>

