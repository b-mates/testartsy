@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Gallery Artist
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($galleryArtist, ['route' => ['galleryArtists.update', $galleryArtist->id], 'method' => 'patch']) !!}

                        @include('gallery_artists.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection