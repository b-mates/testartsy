<!-- Gallery Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gallery_id', 'Gallery Id:') !!}
    {!! Form::number('gallery_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Artist Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('artist_id', 'Artist Id:') !!}
    {!! Form::number('artist_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('galleryArtists.index') !!}" class="btn btn-default">Cancel</a>
</div>
