<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $galleryArtist->id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $galleryArtist->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $galleryArtist->updated_at !!}</p>
</div>

<!-- Gallery Id Field -->
<div class="form-group">
    {!! Form::label('gallery_id', 'Gallery Id:') !!}
    <p>{!! $galleryArtist->gallery_id !!}</p>
</div>

<!-- Artist Id Field -->
<div class="form-group">
    {!! Form::label('artist_id', 'Artist Id:') !!}
    <p>{!! $galleryArtist->artist_id !!}</p>
</div>

