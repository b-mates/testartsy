<li class="{{ Request::is('artTypes*') ? 'active' : '' }}">
    <a href="{!! route('artTypes.index') !!}"><i class="fa fa-edit"></i><span>Art Types</span></a>
</li>

<li class="{{ Request::is('userProfiles*') ? 'active' : '' }}">
    <a href="{!! route('userProfiles.index') !!}"><i class="fa fa-edit"></i><span>User Profiles</span></a>
</li>

<li class="{{ Request::is('galleries*') ? 'active' : '' }}">
    <a href="{!! route('galleries.index') !!}"><i class="fa fa-edit"></i><span>Galleries</span></a>
</li>

<li class="{{ Request::is('artworks*') ? 'active' : '' }}">
    <a href="{!! route('artworks.index') !!}"><i class="fa fa-edit"></i><span>Artworks</span></a>
</li>

<li class="{{ Request::is('artists*') ? 'active' : '' }}">
    <a href="{!! route('artists.index') !!}"><i class="fa fa-edit"></i><span>Artists</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

<li class="{{ Request::is('galleryArtists*') ? 'active' : '' }}">
    <a href="{!! route('galleryArtists.index') !!}"><i class="fa fa-edit"></i><span>Gallery Artists</span></a>
</li>

